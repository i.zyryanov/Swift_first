//task 1
//не стал делать из фразы константу, по идее может меняться

var milkmanPhrase = "Молоко - это полезно"
print(milkmanPhrase)

//task 2

var milkPrice: Double = 3

//task 3
//поменял константу (let) на переменную (var), и тип с Int на Double

milkPrice = 4.20

/*task 4 (со звездочкой)
Почему не нужно использовать принудительное развертывание (force unwrap ): потому что при разработке возможна ситуация, когда развертываемой переменной (опционалу) может по ошибке присвоиться значение nil и тогда при компиляции возникнет ошибка.
Пример кода с подобной ошибкой:
var homeworkScore: Int?
var hoursSpent: Int = 0
var studentRating = 0
studentRating = homeworkScore!/(1 + hoursSpent)*/

var milkBottleCount: Int? = 20
var profit: Double = 0.0

if let milkBottleCount {
    profit = milkPrice * Double(milkBottleCount)
    print("Выручка: \(profit) ₽")
}

else {
    print("В грузовике нет бутылок")
}

//task 5

var employeesList: [String]

employeesList = ["Иван","Петр","Геннадий","Марфа","Андрей"]

//task 6

var isEveryoneWorkHard: Bool = false
var workingHours: Double = 39

isEveryoneWorkHard = workingHours >= 40

print(isEveryoneWorkHard)
