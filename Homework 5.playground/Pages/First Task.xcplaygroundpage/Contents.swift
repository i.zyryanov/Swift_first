import Foundation

open class Shape {
    
    open func calculateArea() -> Double {
        fatalError("not implemented")
    }
    
    open func calculatePerimeter() -> Double {
        fatalError("not implemented")
    }
}

class Rectangle: Shape {
    private let height: Double
    private let width: Double
    
    init(height: Double, width: Double){
        self.height = height
        self.width = width
    }
    
    override func calculateArea() -> Double {height * width}
    
    override func calculatePerimeter() -> Double {(height + width)*2}
}

class Circle: Shape{
    private let radius: Double
    
    init(radius: Double){
        self.radius = radius
    }
    
    override func calculateArea() -> Double {2 * .pi * radius}
    
    override func calculatePerimeter() -> Double {.pi * pow(radius, 2)}
}

class Square: Shape {
    private let side: Double
    
    init(side: Double){
        self.side = side
    }
    
    override func calculateArea() -> Double {pow(side, 2)}
    
    override func calculatePerimeter() -> Double {side * 4}
}

let shapes: [Shape] = [Square(side: 2), Rectangle(height: 1, width: 2), Circle(radius: 2)]
var area: Double
var perimeter: Double

for shape in shapes{
    print("Площадь:", shape.calculateArea())
    print("Периметр:", shape.calculatePerimeter())
}
