func findIndexWithGenerics<Type: Equatable>(ofItem item: Type, in array: [Type]) -> Int? {
    for (index, value) in array.enumerated() {
        if value == item {
            return index
        }
    }

    return nil
}

let arrayOfString = ["кот", "пес", "лама", "попугай", "черепаха"]
let arrayOfInt = [1, 6, 3, 2, 9]
let arrayOfDouble = [12.5, 1.235, 4.87, 0, 123.03]

findIndexWithGenerics(ofItem: "лама", in: arrayOfString)
findIndexWithGenerics(ofItem: 9, in: arrayOfInt)
findIndexWithGenerics(ofItem: 1.235, in: arrayOfDouble)
