import Foundation

//first task
func makeBuffer() -> (String) -> Void {
    var value = ""
    return {string in
        if string.isEmpty {
            print(value)
        }
        else {
            value += string
        }
    }
}

var buffer = makeBuffer()

buffer("Замыкание")
buffer(" использовать")
buffer(" нужно!")
buffer("")

//second task
func checkPrimeNumber(_ number: Int) -> Bool {
    switch number {
    case ...1:
        return false
    case 2,3:
        return true
    default:
        break
    }

    for i in 2 ... Int(sqrt(Double(number))) {
        if number % i == 0 {
            return false
        }
    }
    
    return true
}

checkPrimeNumber(-1)
checkPrimeNumber(7)
checkPrimeNumber(8)
checkPrimeNumber(13)
